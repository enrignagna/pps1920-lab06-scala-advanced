package u06lab.solution

import u06lab.solution.TicTacToe.find

object TicTacToe extends App {

  sealed trait Player {
    def other: Player = this match {
      case X => O;
      case _ => X
    }

    override def toString: String = this match {
      case X => "X";
      case _ => "O"
    }
  }

  case object X extends Player

  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)

  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = {
    //IMPERATIVA: errata
    //var player = Option.empty[Player];
    //board.foreach(m => if (m.x.equals(x) && m.y.equals(y)) player = Option(m.player))
    //return player

    board.find(m => m.x == x && m.y == y).map(_.player)

  }

  def placeAnyMark(board: Board, player: Player): Seq[Board] =
  //IMPERATIVA: errata
  //    import scala.collection.mutable._
  //    var seqBoard = ListBuffer[Board]()
  //    var tempBoard = List[Mark]()
  //    for (x <- 0 to 2; y <- 0 to 2) {
  //      tempBoard = board.+:(Mark(x, y, player))
  //      seqBoard += tempBoard
  //    }
  //    seqBoard

    for (x <- 0 to 2; y <- 0 to 2
         if find(board, x, y).isEmpty)
      yield Mark(x, y, player) :: board


  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case _ => for (
      game <- computeAnyGame(player.other, moves - 1);
      board <- placeAnyMark(game.head, player)
    ) yield {
      if (isWinnerBoard(game.head)) {
        game
      } else {
        board :: game
      }
    }
  }

  def isWinnerBoard(board: Board): Boolean = {
    var state = false;
    for (i <- 0 to 2) {
      //check columns
      if (checkSamePlayer(board.filter(_.x == i)))
        state = true
      //check rows
      if (checkSamePlayer(board.filter(_.y == i)))
        state = true
    }
    //check diagonals
    val firstDiagonal: List[Mark] = List(board.find(m => m.x == 0 && m.y == 0).orNull, board.find(m => m.x == 1 && m.y == 1).orNull, board.find(m => m.x == 2 && m.y == 2).orNull).filter(_ != null)
    if (checkSamePlayer(firstDiagonal)) state = true

    val secondDiagonal: List[Mark] = List(board.find(m => m.x == 2 && m.y == 0).orNull, board.find(m => m.x == 1 && m.y == 1).orNull, board.find(m => m.x == 0 && m.y == 2).orNull).filter(_ != null)
    if (checkSamePlayer(secondDiagonal)) state = true

    state
  }


  def checkSamePlayer(list: List[Mark]): Boolean = {
    (list.size >= 3) && (list.forall(_.player == O) || list.forall(_.player == X))
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2;
         board <- game.reverse;
         x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) {
        print(" ");
        if (board == game.head) println()
      }
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0, 0, X)), 0, 0)) // Some(X)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 0, 1)) // Some(O)
  println(find(List(Mark(0, 0, X), Mark(0, 1, O), Mark(0, 2, X)), 1, 1)) // None

  // Exercise 2: implement placeAnyMark such that..
  println("Exercise 2")
  printBoards(placeAnyMark(List(), X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0, 0, O)), X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  println("Exercise 3")
  computeAnyGame(O, 9) foreach {
    g =>
      printBoards(g);
      println()
  }
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4(VERY ADVANCED !) -- modify the above one so as to stop each game when someone won !!

}